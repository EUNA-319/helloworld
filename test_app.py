import unittest
from app import app

client = app.test_client()

class Testapp(unittest.TestCase):

    def test_add(self):
        c = client.get('/add?a=3&b=2')
        self.assertEqual(5, int(c.text))
        

    def test_sub(self):
        c = client.get('/sub?a=3&b=2')
        self.assertEqual(1, int(c.text))


if __name__ == "__main__":
    unittest.main()
